# ABD Android Toolbox

A companion library containing common tools for Android development like: validations, MVVM architecture, and more.


## Tools Included


* __Validators__. Powered by [ValidateTor](https://github.com/nisrulz/validatetor)

    * `DOBValidator` - Validates Date of Birth Format
    * `EmailValidator` - Validates Email Format
    * `MemberIdValidator` - Validates MemberId Format
    * `NameValidator` - Validates Name Format
    * `PasswordValidator` - Validates Password Format
    * `FileValidator` - Validates File Integrity using MD5 Checksum


* __Converters__.

    * `DisplayUnitConverter` - Px, Dp. Powered by [Android Units](https://github.com/kevelbreh/androidunits)
    * `LengthConverter` - Inch, Feet, Cm
    * `WeightConverter` - Lb, Kg
    * `HexConverter` - Converts decimal values to hex.


* __LiveData Tools__.

    * `Event` - From [Here](https://github.com/google/iosched/blob/master/shared/src/main/java/com/google/samples/apps/iosched/shared/result/Event.kt)
    * `NonNullObservableField` - From [Here](https://medium.com/meesho-tech/non-null-observablefield-in-kotlin-bd72d31ab54f)

    
* __Date Tools__.
    
    * `DateTimeHelper` - Get current date time in UTC and local timezone
    * `DateTimeExtension` - Date time timezone converter (e.g UTC to GMT+8, etc)

    
* __Network Tools__.

   * `MediaUploader` - Uploads files to an S3 bucket. 
   * `CountingRequestBody` - Wraps Okhttp's RequestBody that supports a listener for tracking upload progress.
   * `InternetChecker` - Checks if there's an internet connection
   * `ConnectionChecker` - Checks if the user is using wifi or mobile data.


* __Unity Tools__.

   * `OBBChecker` - Checks if there's accompanied OBB.
   * `AssetBundleChecker` - Checks if there's an asset bundle file.


* __Location Tools__.

   * `GeocoderHelper` - Wraps the native Geocoder that support success and error listeners.
