package com.abd.androidtoolbox.ui

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class CustomDialogFragment : DialogFragment() {

    companion object {
        const val ARG_TITLE = "CustomDialog.Title"
        const val ARG_MESSAGE = "CustomDialog.Message"
        const val ARG_POSITIVE_BUTTON_TEXT = "CustomDialog.PositiveButtonText"
        const val ARG_NEGATIVE_BUTTON_TEXT = "CustomDialog.NegativeButtonText"
        const val ARG_NEUTRAL_BUTTON_TEXT = "CustomDialog.NeutralButtonText"
        const val ARG_CANCELABLE = "CustomDialog.Cancelable"
    }

    private var customDialogFragmentListener: CustomDialogFragmentListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (targetFragment is CustomDialogFragmentListener) {
            customDialogFragmentListener = targetFragment as CustomDialogFragmentListener
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = arguments?.getString(ARG_TITLE) ?: ""
        val message = arguments?.getString(ARG_MESSAGE) ?: ""
        val positiveButtonText = arguments?.getString(ARG_POSITIVE_BUTTON_TEXT)
        val negativeButtonText = arguments?.getString(ARG_NEGATIVE_BUTTON_TEXT)
        val neutralButtonText = arguments?.getString(ARG_NEUTRAL_BUTTON_TEXT)
        isCancelable = arguments?.getBoolean(ARG_CANCELABLE) ?: false

        activity?.run {
            val alertDialog = AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(isCancelable)
                .create()

            if (!positiveButtonText.isNullOrEmpty()) {
                alertDialog.setButton(
                    DialogInterface.BUTTON_POSITIVE,
                    positiveButtonText
                ) { dialog, _ ->
                    if (dialog == null) return@setButton
                    customDialogFragmentListener?.onPositiveButtonClicked(tag)
                    dialog.dismiss()
                }
            }

            if (!negativeButtonText.isNullOrEmpty()) {
                alertDialog.setButton(
                    DialogInterface.BUTTON_NEGATIVE,
                    negativeButtonText
                ) { dialog, _ ->
                    if (dialog == null) return@setButton
                    customDialogFragmentListener?.onNegativeButtonClicked(tag)
                    dialog.dismiss()
                }
            }

            if (!neutralButtonText.isNullOrEmpty()) {
                alertDialog.setButton(
                    DialogInterface.BUTTON_NEUTRAL,
                    neutralButtonText
                ) { dialog, _ ->
                    if (dialog == null) return@setButton
                    customDialogFragmentListener?.onNeutralButtonClicked(tag)
                    dialog.dismiss()
                }
            }

            return alertDialog
        }

        return super.onCreateDialog(savedInstanceState)
    }

    override fun onDestroyView() {
        customDialogFragmentListener = null
        super.onDestroyView()
    }
}

interface CustomDialogFragmentListener {
    fun onPositiveButtonClicked(tag: String? = null) {}
    fun onNegativeButtonClicked(tag: String? = null) {}
    fun onNeutralButtonClicked(tag: String? = null) {}
}

fun CustomDialogFragment.newInstance(
    title: String?,
    message: String?,
    positiveButtonText: String? = null,
    negativeButtonText: String? = null,
    neutralButtonText: String? = null,
    cancelable: Boolean = false
): CustomDialogFragment {
    arguments = Bundle().apply {
        putString(CustomDialogFragment.ARG_TITLE, title)
        putString(CustomDialogFragment.ARG_MESSAGE, message)
        putString(CustomDialogFragment.ARG_POSITIVE_BUTTON_TEXT, positiveButtonText)
        putString(CustomDialogFragment.ARG_NEGATIVE_BUTTON_TEXT, negativeButtonText)
        putString(CustomDialogFragment.ARG_NEUTRAL_BUTTON_TEXT, neutralButtonText)
        putBoolean(CustomDialogFragment.ARG_CANCELABLE, cancelable)
    }
    return this
}