package com.abd.androidtoolbox.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ImageSpan
import android.util.AttributeSet
import android.view.Gravity
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.google.android.material.button.MaterialButton
import com.abd.androidtoolbox.R
import com.abd.androidtoolbox.converters.DisplayUnitConverter

@SuppressLint("AppCompatCustomView")
class CustomButton(context: Context, attrs: AttributeSet?) : MaterialButton(context, attrs) {
    private lateinit var mainTextPaint: Paint
    private lateinit var subTextPaint: Paint
    private var primaryColor = 0
    private var secondaryColor = 0
    private var highlighted: Boolean
    private var roundedCorners: Boolean
    private var subText: String?
    private var subTextSize = textSize
    private var subTextColor = 0

    // Padding
    private var leftPadding = 0f
    private var rightPadding = 0f
    private var topPadding = 0f
    private var bottomPadding = 0f
    private var padding = 0f
    private var startPadding = 0f
    private var endPadding = 0f

    private var currentSubText = ""
    private var currentText = ""

    // Circular Progress
    private lateinit var circularProgressDrawable: CircularProgressDrawable
    private var circularProgressColor = 0

    private val circularProgressDrawableCallback = object : Drawable.Callback {
        override fun unscheduleDrawable(who: Drawable, what: Runnable) {
        }

        override fun invalidateDrawable(who: Drawable) {
            invalidate()
        }

        override fun scheduleDrawable(who: Drawable, what: Runnable, `when`: Long) {
        }
    }

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CustomButton,
            0,
            0
        ).apply {
            highlighted = getBoolean(R.styleable.CustomButton_highlighted, false)

            primaryColor = getColor(
                R.styleable.CustomButton_primaryColor,
                ContextCompat.getColor(context, android.R.color.black)
            )

            secondaryColor = getColor(
                R.styleable.CustomButton_secondaryColor,
                ContextCompat.getColor(context, android.R.color.white)
            )

            circularProgressColor = getColor(
                R.styleable.CustomButton_circularProgressColor,
                ContextCompat.getColor(context, android.R.color.white)
            )

            roundedCorners = getBoolean(R.styleable.CustomButton_roundedCorners, true)

            cornerRadius = if (roundedCorners) {
                getDimension(R.styleable.CustomButton_cornerRadius, convertDpToPx(8f)).toInt()
            } else {
                0
            }

            strokeWidth =
                getDimension(R.styleable.CustomButton_strokeWidth, convertDpToPx(1.5f)).toInt()

            if (highlighted) {
                setTextColor(secondaryColor)
            } else {
                setTextColor(primaryColor)
            }

            subText = getString(R.styleable.CustomButton_subText)

            subTextColor = getColor(R.styleable.CustomButton_subTextColor, currentTextColor)

            subTextSize =
                getDimension(R.styleable.CustomButton_subTextSize, (textSize * .72).toFloat())

            icon = getDrawable(R.styleable.CustomButton_icon)
            iconGravity = ICON_GRAVITY_TEXT_START

            padding = getDimension(R.styleable.CustomButton_android_padding, 0f)
            startPadding =
                getDimension(R.styleable.CustomButton_android_paddingStart, 0f)
            endPadding =
                getDimension(R.styleable.CustomButton_android_paddingEnd, 0f)
            leftPadding =
                getDimension(R.styleable.CustomButton_android_paddingLeft, startPadding)
            rightPadding =
                getDimension(R.styleable.CustomButton_android_paddingRight, endPadding)
            topPadding =
                getDimension(R.styleable.CustomButton_android_paddingTop, 0f)
            bottomPadding =
                getDimension(R.styleable.CustomButton_android_paddingBottom, 0f)

            gravity = getInt(R.styleable.CustomButton_android_gravity, Gravity.CENTER)
        }

        context.obtainStyledAttributes(attrs, intArrayOf(android.R.attr.textAllCaps)).apply {
            isAllCaps = getBoolean(0, false)
            recycle()
        }

        if (!subText.isNullOrEmpty()) {
            mainTextPaint = Paint()
            subTextPaint = Paint()
            mainTextPaint.textSize = textSize
            mainTextPaint.color = currentTextColor
            mainTextPaint.typeface = typeface
            mainTextPaint.textAlign = Paint.Align.CENTER
            mainTextPaint.isAntiAlias = true

            subTextPaint.textSize = subTextSize
            subTextPaint.typeface = typeface
            subTextPaint.color = subTextColor
            subTextPaint.textAlign = Paint.Align.CENTER
            subTextPaint.isAntiAlias = true
        }

        if (highlighted) {
            backgroundTintList = ColorStateList.valueOf(primaryColor)
        } else {
            strokeColor = ColorStateList.valueOf(primaryColor)
            backgroundTintList = ColorStateList.valueOf(secondaryColor)
            val intPadding = padding.toInt()
            if (intPadding > 0) {
                setPadding(
                    paddingLeft + intPadding,
                    paddingTop + intPadding,
                    paddingRight + intPadding,
                    paddingBottom + intPadding
                )
            } else {
                setPadding(
                    paddingLeft + leftPadding.toInt(),
                    paddingTop + topPadding.toInt(),
                    paddingRight + rightPadding.toInt(),
                    paddingBottom + bottomPadding.toInt()
                )
            }
        }

        currentSubText = subText ?: ""
        currentText = text.toString()
    }

    fun showCircularProgress(caption: String = "", marginFromCaption: Int = 0) {
        currentSubText = subText ?: ""
        currentText = text.toString()

        if (::circularProgressDrawable.isInitialized) {
            circularProgressDrawable.stop()
            circularProgressDrawable.callback = null
        }
        circularProgressDrawable = CircularProgressDrawable(context).apply {
            setStyle(CircularProgressDrawable.DEFAULT)
            setColorSchemeColors(circularProgressColor)

            val size = (centerRadius + strokeWidth).toInt() * 2
            setBounds(0, 0, size, size)
        }
        val drawableSpan = CustomDrawableSpan(circularProgressDrawable, marginFromCaption)
        var displayedText = " "
        if (caption.isNotBlank() && caption.isNotEmpty()) {
            displayedText = "$caption "
        }

        val spannableString = SpannableString(displayedText).apply {
            setSpan(
                drawableSpan,
                length - 1,
                length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        circularProgressDrawable.start()

        circularProgressDrawable.callback = circularProgressDrawableCallback

        text = spannableString
        subText = ""
        isEnabled = false
        alpha = 0.75f
    }

    fun hideCircularProgress() {
        if (::circularProgressDrawable.isInitialized) {
            circularProgressDrawable.stop()
            circularProgressDrawable.callback = null
        }
        text = currentText
        subText = currentSubText
        isEnabled = true
        alpha = 1f
    }

    override fun onDraw(canvas: Canvas) {
        if (!subText.isNullOrEmpty()) {
            val xCenterCoordinate = (width * .5).toFloat()
            val mainTextYCoordinates = (height * .48).toFloat()
            val subTextYCoordinates = (height * .78).toFloat()
            canvas.drawText(
                text.toString(),
                xCenterCoordinate,
                mainTextYCoordinates,
                mainTextPaint
            )
            canvas.drawText(
                subText ?: "",
                xCenterCoordinate,
                subTextYCoordinates,
                subTextPaint
            )
        } else {
            super.onDraw(canvas)
        }
    }

    private fun convertDpToPx(dp: Float): Float {
        val displayUnitConverter = DisplayUnitConverter()
        return displayUnitConverter.convertDpToPx(dp)
    }

    inner class CustomDrawableSpan(
        drawable: Drawable,
        private val marginFromCaption: Int = 0
    ) : ImageSpan(drawable) {
        override fun getSize(
            paint: Paint,
            text: CharSequence?,
            start: Int,
            end: Int,
            fontMetricsInt: Paint.FontMetricsInt?
        ): Int {
            // get drawable dimensions
            val rect = drawable.bounds
            fontMetricsInt?.let {
                val fontMetrics = paint.fontMetricsInt

                // get a font height
                val lineHeight = fontMetrics.bottom - fontMetrics.top

                // make sure our drawable has height >= font height
                val drHeight = Math.max(lineHeight, rect.bottom - rect.top)

                val centerY = fontMetrics.top + lineHeight / 2

                // adjust font metrics to fit our drawable size
                fontMetricsInt.apply {
                    ascent = centerY - drHeight / 2
                    descent = centerY + drHeight / 2
                    top = ascent
                    bottom = descent
                }
            }

            return rect.width() + marginFromCaption
        }

        override fun draw(
            canvas: Canvas,
            text: CharSequence?,
            start: Int,
            end: Int,
            x: Float,
            top: Int,
            y: Int,
            bottom: Int,
            paint: Paint
        ) {
            canvas.save()
            val fontMetrics = paint.fontMetricsInt
            // get font height. in our case now it's drawable height
            val lineHeight = fontMetrics.bottom - fontMetrics.top

            // adjust canvas vertically to draw drawable on text vertical center
            val centerY = y + fontMetrics.bottom - lineHeight / 2
            val transY = centerY - drawable.bounds.height() / 2

            // adjust canvas horizontally to draw drawable with defined margin from text
            canvas.translate(x + marginFromCaption, transY.toFloat())

            drawable.draw(canvas)

            canvas.restore()
        }
    }
}