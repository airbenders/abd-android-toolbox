package com.abd.androidtoolbox.unity

import android.content.Context
import com.abd.androidlogger.ABDLogger
import java.io.File
import java.io.FileFilter
import java.util.regex.Pattern

class OBBChecker {

    companion object {
        private const val TAG = "OBBChecker"
    }

    fun hasOBB(context: Context, versionCode: Int, packageName: String): Boolean {
        val obbDir = context.obbDir
        val file = "main.$versionCode.$packageName.obb"
        ABDLogger.d(TAG, "Checking OBB Directory: " + obbDir.absolutePath)
        ABDLogger.d(TAG, "Checking for File (like): $file")
        if (obbDir.exists()) {
            if (obbDir.isDirectory) {
                val files = obbDir.listFiles(
                    OBBFilter(
                        packageName
                    )
                )
                if (files != null && files.isNotEmpty()) {
                    return true
                } else {
                    ABDLogger.e(TAG, "OBB file not found.")
                }
            } else {
                ABDLogger.e(TAG, "OBB path isn't a directory.")
            }
        } else {
            ABDLogger.e(TAG, "OBB directory doesn't exists.")
        }
        return false
    }

    class OBBFilter(packageName: String) : FileFilter {
        private val pattern: Pattern

        init {
            val escapedPackage = packageName.replace(".", "\\.")
            val regex = "main\\.\\d+\\.$escapedPackage\\.obb"
            pattern = Pattern.compile(regex)
            ABDLogger.v(TAG, "Regular Expression: $regex")
        }

        override fun accept(pathname: File): Boolean {
            val filepath = pathname.absolutePath
            ABDLogger.v(TAG, "Verifying File: $filepath")

            return if (pattern.matcher(filepath).find()) {
                ABDLogger.i(TAG, "OBB File Found: $filepath")
                true
            } else {
                false
            }
        }
    }
}