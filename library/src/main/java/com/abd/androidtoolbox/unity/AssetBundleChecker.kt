package com.abd.androidtoolbox.unity

import android.content.Context
import com.abd.androidlogger.ABDLogger
import java.io.File

class AssetBundleChecker {

    companion object {
        private const val TAG = "AssetBundleChecker"
    }

    fun hasAssetBundle(context: Context, bundleName: String): Boolean {
        val dataDir = context.getExternalFilesDir(null)
        ABDLogger.d(TAG, "Checking data directory: ${dataDir?.absolutePath}")
        ABDLogger.d(TAG, "Checking for file: $bundleName")
        if (dataDir?.exists() == true) {
            if (dataDir.isDirectory) {
                val file = File("${dataDir.absolutePath}/$bundleName")
                if (file.exists()) {
                    ABDLogger.i(TAG, "Asset bundle file found: ${file.absolutePath}")
                    return true
                } else {
                    ABDLogger.e(TAG, "Asset bundle file not found.")
                }
            } else {
                ABDLogger.e(TAG, "Data path isn't a directory.")
            }
        } else {
            ABDLogger.e(TAG, "Data directory doesn't exists.")
        }
        return false
    }
}