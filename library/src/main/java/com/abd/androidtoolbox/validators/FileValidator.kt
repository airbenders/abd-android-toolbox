package com.abd.androidtoolbox.validators

import java.io.File
import java.io.FileInputStream
import java.math.BigInteger
import java.security.MessageDigest

/**
 * Source:
 * https://github.com/CyanogenMod/android_packages_apps_CMUpdater/blob/cm-10.2/src/com/cyanogenmod/updater/utils/MD5.java
 */
class FileValidator {

    fun isFileValid(hash: String, file: File): Boolean {
        val inputStream = FileInputStream(file)

        return try {
            val digest: MessageDigest = MessageDigest.getInstance("MD5")
            val buffer = ByteArray(8192)
            var read: Int

            while (inputStream.read(buffer).also { read = it } > 0) {
                digest.update(buffer, 0, read)
            }

            val md5Sum = digest.digest()
            val bigInt = BigInteger(1, md5Sum)
            val output = bigInt.toString(16)
            val formatOutput =
                String.format("%32s", output).replace(" ", "0")

            formatOutput.equals(hash, true)
        } catch (exception: Exception) {
            exception.printStackTrace()
            false
        } finally {
            inputStream.close()
        }
    }
}