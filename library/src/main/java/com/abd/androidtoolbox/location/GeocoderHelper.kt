package com.abd.androidtoolbox.location

import android.content.Context
import android.location.Address
import android.location.Geocoder
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class GeocoderHelper(context: Context) {
    private var geocoder: Geocoder = Geocoder(context)

    fun isAvailable(): Boolean = Geocoder.isPresent()

    fun getLocationByKeyword(
        keyword: String,
        geoCoderListener: GeoCoderListener,
        maxResults: Int = 10
    ) {
        GlobalScope.launch {
            try {
                val geocoderResults = withContext(Dispatchers.IO) {
                    geocoder.getFromLocationName(keyword, maxResults)
                }
                geoCoderListener.onSuccess(geocoderResults)
            } catch (exception: Exception) {
                geoCoderListener.onError(exception)
            }
        }
    }

    fun getLocationByCoords(
        latitude: Double,
        longitude: Double,
        geoCoderListener: GeoCoderListener,
        maxResults: Int = 10
    ) {
        GlobalScope.launch {
            try {
                val geocoderResults = withContext(Dispatchers.IO) {
                    geocoder.getFromLocation(latitude, longitude, maxResults)
                }
                geoCoderListener.onSuccess(geocoderResults)
            } catch (exception: Exception) {
                geoCoderListener.onError(exception)
            }
        }
    }
}

interface GeoCoderListener {
    fun onSuccess(results: List<Address>)
    fun onError(exception: Exception)
}