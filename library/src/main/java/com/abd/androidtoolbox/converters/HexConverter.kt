package com.abd.androidtoolbox.converters

class HexConverter {

    fun convertFromLong(value: Long, padding: Int = 4): String {
        val hexValue = String.format("%0${padding}X", value);
        return if (padding > 4) {
            groupBy4(hexValue);
        } else hexValue
    }

    fun convertFromInt(value: Int?, padding: Int = 4): String {
        return if (value == null) convertFromLong(0, padding)
        else convertFromLong(value.toLong(), padding)
    }

    fun groupBy4(value: String): String {
        val removedSpaces = value.replace("\\s".toRegex(), "")
        val groupBy = 4

        var grouped = ""
        var groupCurrent = 0
        for (i in (removedSpaces.length - 1) downTo 0) {
            if (grouped.isNotBlank() && groupCurrent % groupBy == 0) {
                grouped = " $grouped"
                groupCurrent = 0
            }
            grouped = removedSpaces[i] + grouped
            groupCurrent++
        }

        return grouped
    }

}