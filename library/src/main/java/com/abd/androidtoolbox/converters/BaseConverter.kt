package com.abd.androidtoolbox.converters

import java.text.DecimalFormat

open class BaseConverter {
    val decimalFormat = DecimalFormat("#.0")
}