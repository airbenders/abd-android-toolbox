package com.abd.androidtoolbox.network

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.TRANSPORT_CELLULAR
import android.net.wifi.WifiManager

class ConnectionChecker(private val context: Context) {

    fun isOnWifi(): Boolean {
        val wifiManager = context.applicationContext.getSystemService(WIFI_SERVICE) as WifiManager

        return if (wifiManager.isWifiEnabled) {
            wifiManager.connectionInfo.networkId != -1
        } else {
            false
        }
    }

    fun isOnMobileData(): Boolean {
        val connectivityManager =
            context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networks = connectivityManager.allNetworks

        return networks.any {
            val capabilities = connectivityManager.getNetworkCapabilities(it)
            capabilities?.hasTransport(TRANSPORT_CELLULAR) == true
        }
    }
}