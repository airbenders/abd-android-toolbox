package com.abd.androidtoolbox.network

import android.content.Context
import com.abd.androidlogger.ABDLogger
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.services.s3.AmazonS3Client
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.util.UUID
import java.util.concurrent.TimeUnit

class MediaUploader(
    private val context: Context? = null,
    private val preSignApiUrl: String = "",
    var accessToken: String = "",
    private val s3AccessKey: String = "",
    private val s3SecretKey: String = "",
    private val region: String = "",
    private val bucket: String = ""
) {
    private val client = OkHttpClient.Builder()
        .connectTimeout(20, TimeUnit.SECONDS)
        .writeTimeout(0, TimeUnit.SECONDS)
        .readTimeout(5, TimeUnit.MINUTES)
        .build()

    private var uploadPreSignedJob: Job? = null
    private var uploadPreSignedCall: Call? = null
    private var uploadPreSignedShouldCancel = false

    companion object {
        private const val OBJECT_URL = "https://%s.s3.%s.amazonaws.com/%s"
        private const val PUT = "PUT"
        private const val POST = "POST"
        private const val ACCEPT = "Accept"
        private const val CONTENT_TYPE = "Content-Type"
        private const val APPLICATION_JSON = "application/json"
        private const val AUTHORIZATION = "Authorization"
        private const val TOKEN_FORMAT = "Bearer %s"
        private const val DESTINATION_URL = "destinationUrl"
        private const val SIGNED_URL = "signedUrl"
        private const val TAG = "MediaUploader"
    }

    fun upload(file: File, listener: MediaUploadListener) {
        val basicAWSCredentials = BasicAWSCredentials(s3AccessKey, s3SecretKey)
        val s3Client = AmazonS3Client(basicAWSCredentials, Region.getRegion(region))
        val s3FileKey = UUID.randomUUID().toString()

        val transferUtility = TransferUtility.builder()
            .context(context)
            .awsConfiguration(AWSMobileClient.getInstance().configuration)
            .s3Client(s3Client)
            .build()

        val uploadObserver = transferUtility.upload(s3FileKey, file)

        uploadObserver?.setTransferListener(object : TransferListener {
            override fun onError(id: Int, exception: Exception?) {
                exception?.let { transferException ->
                    transferException.printStackTrace()
                    listener.onError(transferException)
                    ABDLogger.e(TAG, "onError: ${transferException.localizedMessage}")
                }
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                val progress = (((bytesCurrent.toDouble() / bytesTotal) * 100.0).toInt())
                listener.onProgressChanged(progress)
            }

            override fun onStateChanged(id: Int, state: TransferState?) {
                when (state) {
                    TransferState.COMPLETED -> {
                        listener.onTransferCompleted(OBJECT_URL.format(bucket, region, s3FileKey))
                    }
                    TransferState.CANCELED,
                    TransferState.FAILED -> listener.onError(Exception("Transfer ${state.name}"))
                    else -> {
                    }
                }
                ABDLogger.d(TAG, "onStateChanged: State - ${state?.name}")
            }
        })
    }

    fun uploadPreSigned(
        file: File,
        listener: MediaUploadListener,
        requestBody: String = "",
        headers: List<Pair<String, String>>? = null
    ) {
        uploadPreSignedJob = CoroutineScope(Dispatchers.IO).launch {
            try {
                /**
                 * We first get our pre signed credentials and check if signedUrl is empty,
                 * if empty, then we no longer proceed to make the request for uploading
                 * our file.
                 */
                val preSignedCredentials = getPreSignedCredentials(requestBody)
                if (preSignedCredentials.signedUrl.isEmpty()) {
                    listener.onError(Exception("No pre signed url provided."))
                    ABDLogger.e(TAG, "No pre signed credentials fetched")
                    cancel()
                }

                /**
                 * Here we make our request for uploading the file given
                 * a valid pre signed url, this is independent of any app
                 * server, we don't need to add headers.
                 */
                val countingRequestBody = CountingRequestBody(
                    file.asRequestBody(),
                    object : CountingRequestBody.CountingRequestBodyProgressListener {
                        override fun onProgressChanged(
                            bytesWritten: Long,
                            contentLength: Long
                        ) {
                            val progress =
                                (((bytesWritten.toDouble() / contentLength) * 100.0).toInt())
                            listener.onProgressChanged(progress)
                        }
                    })

                val requestBuilder = Request.Builder()
                    .url(preSignedCredentials.signedUrl)
                    .method(PUT, countingRequestBody)

                headers?.forEach { headerPair ->
                    requestBuilder.addHeader(headerPair.first, headerPair.second)
                }

                uploadPreSignedCall = client.newCall(requestBuilder.build())

                uploadPreSignedCall?.enqueue(object : Callback {
                    override fun onResponse(call: Call, response: Response) {
                        if (response.isSuccessful) {
                            listener.onTransferCompleted(preSignedCredentials.objectUrl)
                            ABDLogger.d(TAG, "Upload with preSignedUrl - Successful")
                        } else {
                            response.run {
                                listener.onError(Exception(message))
                                ABDLogger.e(TAG, "Status Code: $code Message: $message")
                                cancel()
                            }
                        }
                    }

                    override fun onFailure(call: Call, e: IOException) {
                        listener.onError(e)
                        ABDLogger.e(
                            TAG,
                            "Upload with preSignedUrl - IOException: ${e.localizedMessage}"
                        )
                        cancel()
                    }
                })

                if (uploadPreSignedShouldCancel) {
                    uploadPreSignedCall?.cancel()
                    uploadPreSignedCall = null
                    uploadPreSignedShouldCancel = false
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
                listener.onError(exception)
                ABDLogger.e(
                    TAG,
                    "Upload with preSignedUrl - Exception: ${exception.localizedMessage}"
                )
                cancel()
            }
        }
    }

    fun cancelUploadPreSigned() {
        val jobShouldCancel =
            uploadPreSignedJob?.run { isActive && !isCancelled && !isCompleted } ?: false
        if (jobShouldCancel) uploadPreSignedJob?.cancel()

        val callShouldCancel =
            uploadPreSignedCall != null && uploadPreSignedCall?.isExecuted() == true
        if (callShouldCancel) {
            ABDLogger.i(TAG, "Call for uploadPreSigned has been initialized, cancelling request")
            uploadPreSignedCall?.cancel()
            uploadPreSignedCall = null
        } else {
            uploadPreSignedShouldCancel = true
        }
    }

    @Throws(Exception::class)
    private fun getPreSignedCredentials(requestBody: String): PreSignedCredentials {
        val requestBuilder = preSignApiUrl.run {
            Request.Builder()
                .url(this)
                .addHeader(ACCEPT, APPLICATION_JSON)
                .addHeader(CONTENT_TYPE, APPLICATION_JSON)
                .addHeader(AUTHORIZATION, TOKEN_FORMAT.format(accessToken))
                .method(POST, requestBody.toRequestBody())
        }

        val response = client.newCall(requestBuilder.build()).execute()
        val jsonResponse = response.body?.string()?.let { JSONObject(it) }
        return PreSignedCredentials(
            jsonResponse!!.getString(DESTINATION_URL),
            jsonResponse.getString(SIGNED_URL)
        )
    }
}

interface MediaUploadListener {
    fun onProgressChanged(progress: Int)
    fun onTransferCompleted(objectUrl: String)
    fun onError(exception: Exception)
}

data class PreSignedCredentials(val objectUrl: String = "", val signedUrl: String = "")