package com.abd.androidtoolbox.validators

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class EmailValidatorTest {
    private val emailValidator = EmailValidator()

    @Test
    fun testValidationInvalidEmailNull() {
        assertFalse(emailValidator.isEmailValid(null, false))
    }

    @Test
    fun testValidationValidEmail() {
        assertTrue(emailValidator.isEmailValid("local@domain.tld", false))
    }

    @Test
    fun testValidationValidEmailCapital() {
        assertTrue(emailValidator.isEmailValid("Local@domain.tld", false))
    }

    // QA ==========================================================================================

    @Test
    fun testValidationInvalidEmailNoDomain() {
        assertFalse(emailValidator.isEmailValid("test", false))
    }

    @Test
    fun testValidationInvalidEmailAtOnly() {
        assertFalse(emailValidator.isEmailValid("test@", false))
    }

    @Test
    fun testValidationInvalidEmailDoubleAtNoTLD() {
        assertFalse(emailValidator.isEmailValid("test@test@", false))
    }

    @Test
    fun testValidationInvalidEmailMissingDomain() {
        assertFalse(emailValidator.isEmailValid("test@.com", false))
    }

    @Test
    fun testValidationInvalidEmailDoubleAt() {
        assertFalse(emailValidator.isEmailValid("test@test.com@", false))
    }

    // Test Values from https://stackoverflow.com/questions/8204680/java-regex-email/13013056#13013056
    // VALID =======================================================================================
    @Test
    fun testValidationValidEmailStackoverflow01() {
        assertTrue(emailValidator.isEmailValid("john@example.commm", false))
    }

    @Test
    fun testValidationValidEmailStackoverflow02() {
        assertTrue(emailValidator.isEmailValid("abc.xyz@gmail", false))
    }

    @Test
    fun testValidationValidEmailStackoverflow03() {
        assertTrue(emailValidator.isEmailValid("email@email", false))
    }

    @Test
    fun testValidationValidEmailStackoverflow04() {
        assertTrue(emailValidator.isEmailValid("65@45", false))
    }

    // INVALID =====================================================================================

    @Test
    fun testValidationInvalidEmailStackoverflow01() {
        assertFalse(emailValidator.isEmailValid("me@example..com", false))
    }

    @Test
    fun testValidationInvalidEmailStackoverflow02() {
        assertFalse(emailValidator.isEmailValid("..@email.com", false))
    }

    // Test Values from Wikipedia: https://en.wikipedia.org/wiki/Email_address#Examples
    // VALID =======================================================================================

    @Test
    fun testValidationValidEmailWikipedia01() {
        assertTrue(emailValidator.isEmailValid("simple@example.com", false))
    }

    @Test
    fun testValidationValidEmailWikipedia02() {
        assertTrue(emailValidator.isEmailValid("very.common@example.com", false))
    }

    @Test
    fun testValidationValidEmailWikipedia03() {
        assertTrue(
            emailValidator.isEmailValid(
                "disposable.style.email.with+symbol@example.com",
                false
            )
        )
    }

    @Test
    fun testValidationValidEmailWikipedia04() {
        assertTrue(emailValidator.isEmailValid("other.email-with-hyphen@example.com", false))
    }

    @Test
    fun testValidationValidEmailWikipedia05() {
        assertTrue(emailValidator.isEmailValid("fully-qualified-domain@example.com", false))
    }

    @Test
    fun testValidationValidEmailWikipedia06() {
        assertTrue(emailValidator.isEmailValid("user.name+tag+sorting@example.com", false))
    }

    @Test
    fun testValidationValidEmailWikipedia07() {
        assertTrue(emailValidator.isEmailValid("user.name@example.com", false))
    }

    @Test
    fun testValidationValidEmailWikipedia08() {
        assertTrue(emailValidator.isEmailValid("x@example.com", false))
    }

    @Test
    fun testValidationValidEmailWikipedia09() {
        assertTrue(
            emailValidator.isEmailValid(
                "\"very.(),:;<>[]\\\".VERY.\\\"very@\\\\ \\\"very\\\".unusual\"@strange.example.com",
                false
            )
        )
    }

    @Test
    fun testValidationValidEmailWikipedia10() {
        assertTrue(emailValidator.isEmailValid("example-indeed@strange-example.com", false))
    }

    @Test
    fun testValidationValidEmailWikipedia11() {
        assertTrue(emailValidator.isEmailValid("#!\$%&'*+-/=?^_`{}|~@example.org", false))
    }

    @Test
    fun testValidationValidEmailWikipedia12() {
        assertTrue(
            emailValidator.isEmailValid(
                "\"()<>[]:,;@\\\\\\\"!#\$%&'-/=?^_`{}| ~.a\"@example.org",
                false
            )
        )
    }

    @Test
    fun testValidationValidEmailWikipedia13() {
        assertTrue(emailValidator.isEmailValid("example@s.example", false))
    }

    @Test
    fun testValidationValidEmailWikipedia14() {
        assertTrue(emailValidator.isEmailValid("user@[2001:DB8::1]", false))
    }

    @Test
    fun testValidationValidEmailWikipedia15() {
        assertTrue(emailValidator.isEmailValid("\" \"@example.org", false))
    }

    // INVALID =====================================================================================

    @Test
    fun testValidationInvalidEmailWikipedia01() {
        assertFalse(emailValidator.isEmailValid("Abc.example.com", false))
    }

    @Test
    fun testValidationInvalidEmailWikipedia02() {
        assertFalse(emailValidator.isEmailValid("A@b@c@example.com", false))
    }

    @Test
    fun testValidationInvalidEmailWikipedia03() {
        assertFalse(emailValidator.isEmailValid("a\"b(c)d,e:f;g<h>i[j\\k]l@example.com", false))
    }

    @Test
    fun testValidationInvalidEmailWikipedia04() {
        assertFalse(emailValidator.isEmailValid("just\"not\"right@example.com", false))
    }

    @Test
    fun testValidationInvalidEmailWikipedia05() {
        assertFalse(emailValidator.isEmailValid("this is\"not\\allowed@example.com", false))
    }

    @Test
    fun testValidationInvalidEmailWikipedia06() {
        assertFalse(
            emailValidator.isEmailValid(
                "this\\ still\\\"not\\\\allowed@example.com",
                false
            )
        )
    }

    @Test
    fun testValidationInvalidEmailWikipedia07() {
        val email = "1234567890123456789012345678901234567890123456789012345678901234+x@example.com"
        assertFalse(emailValidator.isEmailValid(email, false))
    }

    @Test
    fun testValidationInvalidEmailWikipedia08() {
        assertFalse(emailValidator.isEmailValid("john..doe@example.com", false))
    }

    @Test
    fun testValidationInvalidEmailWikipedia09() {
        assertFalse(emailValidator.isEmailValid("john.doe@example..com", false))
    }

    /**
     * Email Validation with required TLD, following LDH(letter, digit, hyphen)
     * rule for domain names. It must match the requirements for a hostname, a list of
     * dot-separated DNS labels, each label being limited to a length of 63 characters
     *
     * 1) uppercase and lowercase Latin letters A to Z and a to z;
     * 2) digits 0 to 9, provided that top-level domain names are not all-numeric;
     * 3) hyphen -, provided that it is not the first or last character.
     */
    @Test
    fun testValidationValidEmailTLDRequiredAlphaNumeric() {
        assertTrue(emailValidator.isEmailValid("john.doe@example.a1"))
    }

    @Test
    fun testValidationValidEmailTLDRequiredMultipleTLD() {
        assertTrue(emailValidator.isEmailValid("john.doe@example.com.au"))
    }

    @Test
    fun testValidationValidEmailTLDRequiredHyphenated() {
        assertTrue(emailValidator.isEmailValid("john.doe@example.c-m"))
    }

    @Test
    fun testValidationValidEmailTLDRequiredLengthyTLD() {
        assertTrue(emailValidator.isEmailValid("john.doe@example.comcomcom"))
    }

    @Test
    fun testValidationInvalidEmailTLDRequiredEndsWithDot() {
        assertFalse(emailValidator.isEmailValid("john.doe@example.a1."))
    }

    @Test
    fun testValidationInvalidEmailTLDRequiredWithConsecutiveDots() {
        assertFalse(emailValidator.isEmailValid("john.doe@example..com.au"))
    }

    @Test
    fun testValidationInvalidEmailTLDRequiredNumericOnly() {
        assertFalse(emailValidator.isEmailValid("john.doe@example.111"))
    }

    @Test
    fun testValidationInvalidEmailTLDRequiredHyphenatedStart() {
        assertFalse(emailValidator.isEmailValid("john.doe@example.-com"))
        assertFalse(emailValidator.isEmailValid("john.doe@example.com.-io"))
    }

    @Test
    fun testValidationInvalidEmailTLDRequiredHyphenatedEnd() {
        assertFalse(emailValidator.isEmailValid("john.doe@example.com.au-"))
    }

    /**
     * Test label length at 63 characters max per label.
     */

    @Test
    fun testValidationValidEmail63TLDLength() {
        assertTrue(
            emailValidator.isEmailValid(
                "john.doe@example" +
                        ".Mel56dJ9PDBVTnzw59INNP9mx5Ufn16HeWuynF8bHbTQBsW6SlxcuSdPnthjWms" +
                        ".BYp0sUE8pFg6Kv39zvKpuj8H7Q8eiolzMwNnB1UOTYgVnkhAq6f9zO6w4lYUmz0"
            )
        )
    }

    @Test
    fun testValidationInvalidEmail63TLDLength() {
        assertFalse(
            emailValidator.isEmailValid(
                "john.doe@example" +
                        ".Mel56dJ9PDBVTnzw59INNP9mx5Ufn16HeWuynF8bHbTQBsW6SlxcuSdPnthjWms" +
                        ".BYp0sUE8pFg6Kv39zvKpuj8H7Q8eiolzMwNnB1UOTYgVnkhAq6f9zO6w4lYUmz0A"
            )
        )
    }

    /**
     * Test domain length at 255 characters max starting from @ symbol.
     */

    @Test
    fun testValidationValidEmail255DomainLength() {
        assertTrue(
            emailValidator.isEmailValid(
                "john.doe@example" +
                        ".VliRL8ULbjjguSa83Ick8kE1d2acawKIYYJqiESex7P5ujEyuEkPmflB9SuQG" +
                        ".4cKSnSO1SJJQhqt2cew9SG49d26CkDZ5iVflqH1byhRN3Yi7DzKk4vrhiQK1b" +
                        ".7dQQ3hQpxcRvNzKwWERZRY9FubZOQVfwYtfXjuBb1CqCS4oXdTKldBvv2ljex" +
                        ".kA0YC6N83n1RxHJwit6AR86GhLODoCrsJiGbYvEqihqjdhvIZrOPupLO900ef"
            )
        )
    }

    @Test
    fun testValidationInvalidEmail255DomainLength() {
        assertFalse(
            emailValidator.isEmailValid(
                "john.doe@longexample" +
                        ".VliRL8ULbjjguSa83Ick8kE1d2acawKIYYJqiESex7P5ujEyuEkPmflB9SuQG" +
                        ".4cKSnSO1SJJQhqt2cew9SG49d26CkDZ5iVflqH1byhRN3Yi7DzKk4vrhiQK1b" +
                        ".7dQQ3hQpxcRvNzKwWERZRY9FubZOQVfwYtfXjuBb1CqCS4oXdTKldBvv2ljex" +
                        ".kA0YC6N83n1RxHJwit6AR86GhLODoCrsJiGbYvEqihqjdhvIZrOPupLO900ef"
            )
        )
    }
}