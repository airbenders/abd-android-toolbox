package com.abd.androidtoolbox.converters

import org.junit.Assert.assertEquals
import org.junit.Test

class HexConverterTest {

    private val hexConverter = HexConverter()

    @Test
    fun testTrimming() {
        val result = hexConverter.groupBy4(" A ")
        assertEquals("A", result)
    }

    @Test
    fun testRemovalOfSpaces() {
        val result = hexConverter.groupBy4(" A B ")
        assertEquals("AB", result)
    }

    @Test
    fun testNoNeedOfGrouping() {
        val result = hexConverter.groupBy4("ABCD")
        assertEquals("ABCD", result)
    }

    @Test
    fun testGroupingNeeded() {
        val result = hexConverter.groupBy4("ABCDABCD")
        assertEquals("ABCD ABCD", result)
    }

    @Test
    fun testUnevenGrouping() {
        val result = hexConverter.groupBy4("ABABCD")
        assertEquals("AB ABCD", result)
    }

    @Test
    fun testEmptyGrouping() {
        val result = hexConverter.groupBy4("")
        assertEquals("", result)
    }

    @Test
    fun test12Grouping() {
        val result = hexConverter.groupBy4("ABCDABCDABCD")
        assertEquals("ABCD ABCD ABCD", result)
    }

    @Test
    fun test10Grouping() {
        val result = hexConverter.groupBy4("ABABCDABCD")
        assertEquals("AB ABCD ABCD", result)
    }

    @Test
    fun testHexValueA() {
        val result = hexConverter.convertFromLong(10)
        assertEquals("000A", result)
    }

    @Test
    fun testHexValueB() {
        val result = hexConverter.convertFromLong(11)
        assertEquals("000B", result)
    }

    @Test
    fun testHexValueC() {
        val result = hexConverter.convertFromLong(12)
        assertEquals("000C", result)
    }

    @Test
    fun testHexValueD() {
        val result = hexConverter.convertFromLong(13)
        assertEquals("000D", result)
    }

    @Test
    fun testHexValueE() {
        val result = hexConverter.convertFromLong(14)
        assertEquals("000E", result)
    }

    @Test
    fun testHexValueF() {
        val result = hexConverter.convertFromLong(15)
        assertEquals("000F", result)
    }

    @Test
    fun testIncreasedPadding() {
        val result = hexConverter.convertFromLong(10, padding = 8)
        assertEquals("0000 000A", result)
    }

    @Test
    fun testIncreasedPadding2() {
        val result = hexConverter.convertFromLong(655360, padding = 8)
        assertEquals("000A 0000", result)
    }

    @Test
    fun testLargeValue() {
        val result = hexConverter.convertFromLong(4294967295, padding = 8)
        assertEquals("FFFF FFFF", result)
    }

    @Test
    fun testIntValue() {
        val result = hexConverter.convertFromInt(15)
        assertEquals("000F", result)
    }

    @Test
    fun testNullValue() {
        val result = hexConverter.convertFromInt(null)
        assertEquals("0000", result)
    }
}